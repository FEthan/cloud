from pyecharts.charts import Bar
from pyecharts import options as opts

# 示例数据
cate = ['May 21 11:28:12','May 21 13:03:19','May 21 15:16:16','May 21 17:02:09','May 21 19:22:52','May 21 20:53:41','May 21 21:31:01','May 21 22:31:54','May 21 23:07:46']
data1 = [3617816,3496428,3494072,3489964,3489696,3486268,3484000,3485116,3484804]
data2 = [3779260,3557976,3405640,3397536,3391144,3384932,3379496,3379108,3378220]

bar = (Bar()
       .add_xaxis(cate)
       .add_yaxis('swap_free_memory', data1)
       .add_yaxis('free_memory', data2)
       .set_global_opts(title_opts=opts.TitleOpts(title="云平台实时数据", subtitle="内存数据"))
      )
# 在jupyter notebook总渲染
bar.render()
