from pyecharts.charts import Bar
from pyecharts import options as opts

# 示例数据
cate = ['May21 11:28:12','May21 11:28:21','May21 11:28:35','May21 11:28:43','May21 11:28:52','May21 11:29:01','May21 11:29:10','May21 11:29:19','May21 11:29:27']
data1 = [33539,36155,42143 ,46337 ,46667 ,47009 ,47405 ,47801 ,49165 ,49363]
data2 = [175326 ,179053 ,186164 ,190887 ,193190 ,195381 ,197818 ,200231 ,204030 ,206045 ]

bar = (Bar()
       .add_xaxis(cate)
       .add_yaxis('net_RX', data1)
       .add_yaxis('net_TX', data2)
       .set_global_opts(title_opts=opts.TitleOpts(title="云平台实时数据", subtitle="X-Y轴翻转网络数据"))
       .set_series_opts(label_opts=opts.LabelOpts(is_show=False))
       .reversal_axis()
      )

bar.render()
