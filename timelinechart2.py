from pyecharts import options as opts
from pyecharts.charts import Line, Timeline
import random

# 示例数据
cate = ['5', '15', '25', '35', '45', '55']

tl = Timeline()
for i in range(11, 23):
    bar = (
        Line()
            .add_xaxis(cate)
            .add_yaxis("net_RX", [random.randint(33500, 49300) for _ in cate])
            .add_yaxis("net_TX", [random.randint(175326, 206045) for _ in cate])
            .set_global_opts(title_opts=opts.TitleOpts("云平台{}点网络预测数据".format(i)))
    )
    tl.add(bar, "北京时间{}点".format(i))

tl.render()
