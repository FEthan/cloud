from pyecharts import options as opts
from pyecharts.charts import Bar, Timeline
import random

# 示例数据
cate = ['1', '2', '3', '4', '5', '6']

tl = Timeline()
for i in range(11, 23):
    bar = (
        Bar()
            .add_xaxis(cate)
            .add_yaxis("swap_free_memory", [random.randint(355000, 361000) for _ in cate])
            .add_yaxis("free_memory", [random.randint(365000, 377000) for _ in cate])
            .set_global_opts(title_opts=opts.TitleOpts("云平台{}点预测数据".format(i)))
    )
    tl.add(bar, "北京时间{}点".format(i))

tl.render()
